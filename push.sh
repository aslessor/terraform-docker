#!/bin/bash
# Builds a Docker image and pushes to an AWS ECR repository
# name of the file - push.sh

login_aws_ecr() {
    aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 144589013661.dkr.ecr.us-east-2.amazonaws.com
}
# login_aws_ecr

push_ecr_image() {    
    docker build -t terraform-testing .
    docker tag terraform-testing:latest 144589013661.dkr.ecr.us-east-2.amazonaws.com/terraform-testing:latest
    docker push 144589013661.dkr.ecr.us-east-2.amazonaws.com/terraform-testing:latest
}
# push_ecr_image







# set -e
# source_path="$1" # 1st argument from command line
# repository_url="$2" # 2nd argument from command line
# tag="${3:-latest}" # Checks if 3rd argument exists, if not, use "latest"

# # splits string using '.' and picks 4th item
# region="$(echo "$repository_url" | cut -d. -f4)" 

# # splits string using '/' and picks 2nd item
# image_name="$(echo "$repository_url" | cut -d/ -f2)" 

# # builds docker image
# (cd "$source_path" && docker build -t "$image_name" .) 

# # login to ecr
# $(aws ecr get-login --no-include-email --region "$region") 

# # tag image
# docker tag "$image_name" "$repository_url":"$tag"

# # push image
# docker push "$repository_url":"$tag" 