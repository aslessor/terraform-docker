
TODO:

1. Figure out how to attach ssl certificate

links:

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener


# data "aws_iam_policy_document" "role_policy_data" {
#   assume_role_policy = <<EOF
#   {  
#     "Version": "2012-10-17",
#     "Statement": [
#       {
#         "Effect": "Allow",
#         "Action": ["secretsmanager:GetSecretValue"],
#         "Resource": ["${local.docker_creds_arn}"]
#       }
#     ]
#   } 
#   EOF
# }

  # statement {
  #   actions = ["secretsmanager:GetSecretValue"]
  #   # resources = ["arn:aws:secretsmanager:us-east-2:144589013661:secret:DockerHubAuth-ltoMMs"]
  #   principals {
  #     type        = "AWS"
  #     identifiers = ["arn:aws:iam::144589013661:role:role/Secretassumerole"]
  #     # identifiers = ["arn:aws:secretsmanager:us-east-2:144589013661:secret:DockerHubAuth-ltoMMs"]
  #   }
  # }
  

# Command Overview

__Terraform:__

- terraform init (to initialize the working directory)

- terraform apply ( you can add — auto-approve if you don't want to give confirmations manually.)

- terraform show

- terraform state list

- terraform destroy

__AWS-CLI:__

```
aws acm request-certificate --domain-name thespicey.com
# go to email and verify ownership
```


# Terraform Examples:

To change infastructure:

  - Run `terraform apply` again.



__Setup Docker:__
```tf
provider "docker" {
  host = "unix:///var/run/docker.sock"

  registry_auth {
    address  = "https://hub.docker.com/u/capcombravo"
    username = "slessor1"
    password = "aasdasda"
  }
}

#OR

provider "docker" {
  host = "unix:///var/run/docker.sock"# This is only for linux
  
  registry_auth {
    address  = "https://hub.docker.com/u/slessor1"
    config_file = pathexpand("~/.docker/config.json")
  }
}
```


```tf
resource "docker_image" "capcombravo" {
  name = "${local.image_name}:${local.image_version}"# use this name if pushing to ECR
  # name = "${local.docker_username}/${local.image_name}:${local.image_version}"# use this name if pushing to dockerhub
  build {
    path = "${local.dockerfile_path}"
    force_remove = true
    no_cache     = true
    label = {
      author : "${local.docker_username}"
    }
  }
  provisioner "local-exec" {
    # use this command for pushing to ECR
    command = "${local.ecr_login_cmd} && docker tag ${local.image_name}:${local.image_version} ${aws_ecr_repository.testing.repository_url} && docker push ${aws_ecr_repository.testing.repository_url}"
    # command = "./push.sh && docker push ${local.docker_username}/${local.image_name}:${local.image_version}"

    # This command for docker hub
    # command = "docker push ${local.docker_username}/${local.image_name}:${local.image_version}"
  }
}

# OR

resource "null_resource" "push" {
      provisioner "local-exec" {
        command = "./push.sh"
      #  command     = "${coalesce("push.sh", "${path.module}/push.sh")} ${var.source_path} ${aws_ecr_repository.repo.repository_url} ${var.tag}"
       interpreter = ["bash", "-c"]
    }
}
```


```tf
resource "docker_registry_image" "image_name" {
  # name = "something-amazing/helloworld:2.0"
  name = "https://hub.docker.com/repository/docker/slessor1/terra1"
  build {
    context = "."
  }
}

data "docker_registry_image" "ubuntu" {
  name = "ubuntu:latest"
}

resource "docker_image" "ubuntu" {
  name          = "${data.docker_registry_image.ubuntu.name}"
  pull_triggers = ["${data.docker_registry_image.ubuntu.sha256_digest}"]
}
```

### ECR

CREATING ECR REPO
- terraform init (to initialize the working directory)
- terraform apply ( you can add — auto-approve if you don't want to give confirmations manually.)

__Create ECR Repository:__
```tf
resource "aws_ecr_repository" "testing" {
  name                 = "${local.image_name}"
  image_tag_mutability = "MUTABLE"
}
```

### Load Balancer SSL Certificate
```
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate
resource "aws_lb_listener_certificate" "listener_cert" {
  listener_arn    = aws_lb_listener.listener.arn
  # this is only if you validate new domain
  # certificate_arn = aws_acm_certificate.acm_cert.arn
  # this is for existing certificate
  certificate_arn   = "arn:aws:acm:us-east-2:144589013661:certificate/39679a0a-e12d-4575-b414-9db9d3e36e3d"
}
```



### SEcrets Manager:
A customer master key and an alias in AWS KMS to encrypt your secret

- A secret in AWS Secrets Manager to store your Docker Hub username and password

- An ECS task execution role to give your task permission to decrypt and retrieve your secret
- An ECS cluster and VPC resources using the Amazon ECS CLI
- An Amazon ECS service running one instance of a task on your cluster using the AWS Fargate launch type


```
resource "aws_secretsmanager_secret" "secretmasterDB" {
  name = "DockerHubAuth"
}
resource "aws_secretsmanager_secret_version" "sversion" {
  secret_id     = aws_secretsmanager_secret.secretmasterDB.id
  secret_string = <<EOF
    {
     "username": "slessor1",
     "password": "asAS12!@as"
    }
  EOF
}

# # Lets import the Secrets which got created recently and store it so that we can use later. 
data "aws_secretsmanager_secret" "secretmasterDB" {
  arn = aws_secretsmanager_secret.secretmasterDB.arn
}
data "aws_secretsmanager_secret_version" "creds" {
  secret_id = data.aws_secretsmanager_secret.secretmasterDB.arn
}

output "secrets_output" {
value = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)
value = data.aws_secretsmanager_secret_version.creds.secret_id
}
```


## Errors:
```
ResourceInitializationError: unable to pull secrets or registry auth: execution resource retrieval failed: unable to get registry auth from asm: service call has been retried 1 time(s): failed to fetch secret arn:aws:secretsmanager:us-east-2:1445890136...
```


```
│ Error: Unable to read Docker image into resource: unable to pull image ideaworksmedic/terraform-testing:latest: error pulling image ideaworksmedic/terraform-testing:latest: Error response from daemon: pull access denied for ideaworksmedic/terraform-testing, repository does not exist or may require 'docker login': denied: requested access to the resource is denied
│ 
│   with docker_image.ideaworksmedic,
│   on main.tf line 56, in resource "docker_image" "ideaworksmedic":
│   56: resource "docker_image" "ideaworksmedic" {
│ 
╵
```


CREATING TASK DEFINITION 
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#container_definition_image
IMAGE PARAM DOCS

# Where is my apps URL?:

![url](./final.png)




references:

https://www.terraform.io/language
