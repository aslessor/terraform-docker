

# AWS PROVIDER BLOCK
provider "aws" {
  # version = ">= 1.58.0, <= 2.0.0"
  region = var.aws_region
  # region = "us-east-2"
}

provider "docker" {
  # host = "unix:///var/run/docker.sock"
  host = var.docker_host
  registry_auth {
    # address = "https://hub.docker.com/u/slessor1"
    address     = var.docker_address
    config_file = pathexpand("~/.docker/config.json")
  }
}
