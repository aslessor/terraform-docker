# TERRAFORM PROVIDER BLOCK
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.54.0"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}
locals {
  docker_username            = "ideaworksmedic"
  image_name                 = "terraform-testing"
  image_version              = "latest"
  dockerfile_path            = "."
  iam_role_name              = "terraTestingRole"
  ecs_service_name           = "test-alex-service"
  ecs_cluster_name           = "test-alex-cluster"
  ecs_task_definition_family = "testAppTaskDefinition2"
  target_group_name          = "test-alex-target-group"
  lb_name                    = "test-alex-lb"
  lb_container_port          = 5001 # port app is running on
  ecs_task_memory            = 512
  ecs_task_cpu               = 256

  ecr_login_cmd = "aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 144589013661.dkr.ecr.us-east-2.amazonaws.com"

}

resource "aws_ecr_repository" "testing" {
  name                 = "${local.image_name}"
  image_tag_mutability = "MUTABLE"
}

resource "docker_image" "capcombravo" {
  name = "${local.image_name}:${local.image_version}"# use this name if pushing to ECR
  # name = "${local.docker_username}/${local.image_name}:${local.image_version}"# use this name if pushing to dockerhub
  build {
    path = "${local.dockerfile_path}"
    force_remove = true
    no_cache     = true
    label = {
      author : "${local.docker_username}"
    }
  }
  provisioner "local-exec" {
    # use this command for pushing to ECR
    command = "${local.ecr_login_cmd} && docker tag ${local.image_name}:${local.image_version} ${aws_ecr_repository.testing.repository_url} && docker push ${aws_ecr_repository.testing.repository_url}"
    # command = "./push.sh && docker push ${local.docker_username}/${local.image_name}:${local.image_version}"

    # This command for docker hub
    # command = "docker push ${local.docker_username}/${local.image_name}:${local.image_version}"
  }
}


resource "aws_ecs_task_definition" "testAppTaskDefinition2" {
  family = "testAppTaskDefinition2"#(Required): A unique name for your task definition
  container_definitions    = <<DEFINITION
  [
    {
      "name": "testAppTaskDefinition2",
      "image": "${aws_ecr_repository.testing.repository_url}",
      "essential": true,
      "portMappings": [
        {
          "containerPort": ${local.lb_container_port},
          "hostPort": ${local.lb_container_port}
        }
      ],
      "memory": ${local.ecs_task_memory},
      "cpu": ${local.ecs_task_cpu}
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] # USING FARGATE
  network_mode             = "awsvpc"    # USING AWS VPC
  memory                   = local.ecs_task_memory        # MEMORY OUR CONTAINER REQUIRES
  cpu                      = local.ecs_task_cpu         # CPU OUR CONTAINER REQUIRES
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
}


data "aws_iam_policy_document" "role_policy_data" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# # CREATING REQUIRED IAM ROLE AND STS ASSUME ROLE
resource "aws_iam_role" "ecsTaskExecutionRole" {
  name = "${local.iam_role_name}"
  description        = "Allows ecs to execute external api commands on your behalf"
  assume_role_policy = data.aws_iam_policy_document.role_policy_data.json
}
resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  # description = "Allows the container agent to pull the container image."
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}





# PROVINDING REFERENCE TO OUR DEFAULT VPC AND SUBNETS
resource "aws_default_vpc" "default_vpc" {
}
resource "aws_default_subnet" "default_subnet_a" {
  availability_zone = "${var.aws_region}a"
}
resource "aws_default_subnet" "default_subnet_b" {
  availability_zone = "${var.aws_region}b"
}


# CREATING ECS CLUSTER
resource "aws_ecs_cluster" "ecs_test_cluster" {
  name = local.ecs_cluster_name
}

resource "aws_ecs_service" "ecs_test_service" {
  name            = local.ecs_service_name                             # Naming our first service
  cluster         = aws_ecs_cluster.ecs_test_cluster.id                # Referencing our created Cluster
  task_definition = aws_ecs_task_definition.testAppTaskDefinition2.arn # Referencing the task our service will spin up
  launch_type     = "FARGATE"
  desired_count   = 2 # Setting the number of containers to >=2; min must be 2

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn # Referencing our target group
    container_name   = aws_ecs_task_definition.testAppTaskDefinition2.family
    container_port   = local.lb_container_port # Specifying the container port
  }

  network_configuration {
    subnets = [
      "${aws_default_subnet.default_subnet_a.id}",
      "${aws_default_subnet.default_subnet_b.id}",
    ]
    assign_public_ip = true                                                # Providing our containers with public IPs
    security_groups  = ["${aws_security_group.service_security_group.id}"] # Setting the security group
  }
}


resource "aws_security_group" "service_security_group" {
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = ["${aws_security_group.lb_security_group.id}"]
  }
  egress {
    from_port   = 0             # Allowing any incoming port
    to_port     = 0             # Allowing any outgoing port
    protocol    = "-1"          # Allowing any outgoing protocol 
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
  }
}


# CREATING APPLICATION LOAD BALANCER
resource "aws_lb" "application_load_balancer" {
  name               = local.lb_name
  load_balancer_type = "application"
  subnets = [ #REFERENCE TO OUR AWS DEFAULT SUBNETS
    "${aws_default_subnet.default_subnet_a.id}",
    "${aws_default_subnet.default_subnet_b.id}",
  ]
  security_groups = [ # REFERENCING THE SECURITY GROUP
    "${aws_security_group.lb_security_group.id}"
  ]
}



# CREATING SECURITY GROUP & SETTING UP ROUTES FOR APPLICATION LOAD BALANCER
resource "aws_security_group" "lb_security_group" {
  ingress {
    from_port   = 80 #TRAFFIC ALLOWED FROM PORT
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #ALLOWING TRAFFIC FROM IPS
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# TARGET GROUP FOR ALB AND HEALTH CHECK PATH
resource "aws_lb_target_group" "target_group" {
  name        = local.target_group_name
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_default_vpc.default_vpc.id
  health_check {
    healthy_threshold   = "2"
    unhealthy_threshold = "6"
    interval            = "30"
    matcher             = "200,301,302"
    path                = "/"
    protocol            = "HTTP"
    timeout             = "5"
  }
}

# LB LISTENER PORT
resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.application_load_balancer.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}

# # # # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate
# # resource "aws_lb_listener_certificate" "listener_cert" {
# #   listener_arn    = aws_lb_listener.listener.arn
# #   # this is only if you validate new domain
# #   # certificate_arn = aws_acm_certificate.acm_cert.arn
# #   # this is for existing certificate
# #   certificate_arn   = "arn:aws:acm:us-east-2:144589013661:certificate/39679a0a-e12d-4575-b414-9db9d3e36e3d"
# # }

output "alb_dns_name" {
  value = aws_lb.application_load_balancer.dns_name
}
